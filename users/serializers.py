from django.contrib.auth.models import User
from rest_framework import serializers


class MyUser(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password'
        ]
