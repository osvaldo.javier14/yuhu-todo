from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from . import views

PREFIX_VERSION_API_1 = 'api/v1'

urlpatterns = [
    path(
        'signup/',
        views.SignUp.as_view(), name='signup'),
    path(
        f'{PREFIX_VERSION_API_1}/token/',
        TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path(
        f'{PREFIX_VERSION_API_1}/token/refresh/',
        TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
