from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from utils.paginations import CustomPagination

from .serializers import MyUser as UserSerializer
from yuhu_tasks.serializers import Task as TaskSerializer
from yuhu_tasks.models import Task as TaskModel


class SignUp(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
