#!/bin/bash
set -e

set -o errexit
set -o pipefail
set -o nounset

mkdir -p /src/app/logs


# old
# python manage.py makemigrations
# python manage.py makemigrations yuhu_tasks
# python manage.py makemigrations users
# python manage.py migrate
# # python manage.py collectstatic --noinput
# python manage.py runserver 0.0.0.0:8002


# Prepare log files and start outputting logs to stdout
touch /src/app/gunicorn.log
touch /src/app/access.log
tail -n 0 -f /src/app/*.log &

python manage.py makemigrations
python manage.py migrate

if [ "$ENV" = "dev" ] ; then
    python manage.py runserver 0:8002
else
    python manage.py collectstatic --noinput
    echo Starting Gunicorn.
    exec gunicorn yuhuTodo.wsgi \
        --name core \
        --bind 0.0.0.0:8002 \
        --workers 3 \
        --timeout 120 \
        --worker-class gevent \
        --log-level=info \
        --log-file=/src/app/gunicorn.log \
        --access-logfile=/src/app/access.log \
        "$@"
fi
