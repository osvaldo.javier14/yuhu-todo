# yuhu-todo

## Demo
> [Link al DEMO](http://yuhu-env2.eba-gtx2w4ws.us-west-2.elasticbeanstalk.com/tasks/home)

## Resumen
Se han desarrollado endpoints para la creación de tareas anónimas con la opción de incluir una fecha de expiración (opcional). Cuando se **(crea | actualiza | expira)** una tarea, se envía un correo electrónico de manera asíncrona utilizando **Celery**.

También se han creado endpoints para la creación de tareas con un usuario autenticado. Para la autenticación de los usuarios, se ha implementado la estrategia de tokens JWT, con tokens de actualización y de acceso.

El **_token de acceso_** tiene una caducidad de **15 minutos**, y una vez que expira, es necesario actualizar usando el  **_token de actualización_**, el cual tiene una caducidad de **30 minutos**.

Para permitir el acceso de los usuarios, el **_token de acceso_** debe ser incluido en la cabecera **Authorization** con el valor "**Bearer <access_token>**".


## Modo Dev
> `docker compose up --build`

## Modo Stage
> Se ha empleado PostgreSQL en RDS con Elastic Beanstalk.

## Modo Prod
> En construcción...

## Documentación en Postman

> [Link a la documentación en Postman](https://www.postman.com/oh-team/workspace/yuju-oh/request/774210-f9aed450-1e15-42ed-8c6f-5de97cec8392)

## Reporte de tiempo invertido

> [Link al Wakatime](https://wakatime.com/@e065112b-e947-491f-9bb4-8e089375a35a/projects/dqpkoaryki?start=2024-04-20&end=2024-04-26)

by: Osvaldo Hinojosa 🌻