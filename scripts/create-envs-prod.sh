cp .env.example .env

# reemplazar las variables de entorno en el archivo .env
sed -i 's|<ENV>|prod|g' .env
sed -i 's|<DEBUG>|'"$PROD_DEBUG"'|g' .env
sed -i 's|<SECRET_KEY>|'"$SECRET_KEY"'|g' .env
sed -i 's|<DOMAIN>|'"$PROD_DOMAIN"'|g' .env

sed -i 's|<EMAIL_HOST_USER>|'"$PROD_EMAIL_HOST_USER"'|g' .env
sed -i 's|<EMAIL_HOST_PASSWORD>|'"$PROD_EMAIL_HOST_PASSWORD"'|g' .env

sed -i 's|<REDIS_URI>|'"$PROD_REDIS_URI"'|g' .env

sed -i 's|<AWS_ACCESS_KEY_ID>|'"$AWS_ACCESS_KEY_ID"'|g' .env
sed -i 's|<AWS_SECRET_ACCESS_KEY>|'"$AWS_SECRET_ACCESS_KEY"'|g' .env
sed -i 's|<AWS_STORAGE_BUCKET_NAME>|'"$PROD_AWS_STORAGE_BUCKET_NAME"'|g' .env
sed -i 's|<AWS_S3_CUSTOM_DOMAIN>|'"$PROD_AWS_S3_CUSTOM_DOMAIN"'|g' .env

sed -i 's|<DB_NAME>|'"$PROD_DB_NAME"'|g' .env
sed -i 's|<DB_USER>|'"$PROD_DB_USER"'|g' .env
sed -i 's|<DB_PASSWORD>|'"$PROD_DB_PASSWORD"'|g' .env
sed -i 's|<DB_HOSTNAME>|'"$PROD_DB_HOSTNAME"'|g' .env
sed -i 's|<DB_PORT>|'"$PROD_DB_PORT"'|g' .env
