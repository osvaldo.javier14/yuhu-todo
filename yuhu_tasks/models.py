from django.db import models
from django.contrib.auth.models import User


class Topic(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-pk']


class Task(models.Model):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=255)
    topics = models.ManyToManyField(Topic)
    is_public = models.BooleanField(default=False)
    date_expiration = models.DateTimeField(
        null=True,
        blank=True
    )
    is_expired = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-pk']


class Subscriber(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )
    topics = models.ManyToManyField(Topic)

    def __str__(self):
        return str(self.pk)

    class Meta:
        ordering = ['-pk']
