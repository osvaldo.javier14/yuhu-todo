from .models import Task as TaskModel


def get_all_tasks():
    return TaskModel.objects.filter(
        is_expired=False).order_by('-pk')
