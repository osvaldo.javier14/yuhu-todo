from rest_framework import serializers
from datetime import datetime

from .models import Task


class Task(serializers.ModelSerializer):
    email = serializers.EmailField(required=False)
    date_expiration = serializers.DateTimeField(
        required=False
    )
    # topics required false
    topics = serializers.PrimaryKeyRelatedField(
        required=False,
        many=True,
        read_only=False,
        queryset=Task.topics.field.related_model.objects.all()
    )

    class Meta:
        model = Task
        fields = [
            'id',
            'title',
            'user',
            'email',
            'description',
            'date_expiration',
            'topics',
            'is_public',
        ]

    def validate_email(self, value):
        request = self.context.get('request')
        if request:
            user = request.user
            if user.is_authenticated:
                return user.email
            elif not value:
                raise serializers.ValidationError(
                    "El campo email es requerido.")
        return value
