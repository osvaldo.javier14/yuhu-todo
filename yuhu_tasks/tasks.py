from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail

from .models import Task as TaskModel, Subscriber as SubscriberModel


@shared_task
def send_mail_to(email, title, content):
    print('sending email to =>', email)
    send_mail(
        title,
        content,
        settings.EMAIL_HOST_USER,
        [email],
        fail_silently=False,
    )
    return ('success', 'email enviado')


@shared_task
def expirate_task_to(id):
    # cambiar el estado de la tarea a expirado
    task = TaskModel.objects.get(pk=id)
    task.is_expired = True
    task.save()

    # enviar email de notificacion
    send_mail_to.delay(
        task.email,
        'Tarea expirada',
        'La tarea "{}" ha expirado.'.format(task.title)
    )
    return ('success', 'tarea expirada')


@shared_task
def notify_subscribers(id):
    task = TaskModel.objects.get(pk=id)

    if task.is_public and not task.is_expired:
        topics = task.topics.all()
        subscribers = SubscriberModel.objects.filter(
            topics__in=topics)
        subject = f'Nueva tarea publica: {task.title}'
        message = f'Lee la tarea aqui: your_website_url/posts/{id}'

        for subscriber in subscribers:
            # enviar email de notificacion
            send_mail_to.delay(
                subscriber.user.email,
                subject,
                message
            )
    return ('success', 'noficacion(es) enviada(s)')
