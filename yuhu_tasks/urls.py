from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from yuhu_tasks import views

PREFIX_VERSION_API_1 = 'api/v1'

urlpatterns = [
    path(
        'hello/',
        views.hello
    ),
    path(
        f'',
        views.home
    ),
    path(
        f'{PREFIX_VERSION_API_1}/tasks/',
        views.Task.as_view()
    ),
    path(
        f'{PREFIX_VERSION_API_1}/tasks/<str:pk>/',
        views.Task.as_view()
    ),
    path(
        f'{PREFIX_VERSION_API_1}/tasks/users/my/',
        views.MyTask.as_view()
    )
]

urlpatterns = format_suffix_patterns(urlpatterns)
