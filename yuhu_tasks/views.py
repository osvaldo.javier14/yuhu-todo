from django.shortcuts import render

from datetime import datetime
from django.http import HttpResponse
from rest_framework import status
from rest_framework_api_key.permissions import HasAPIKey
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import render


from .models import Task as TaskModel
from . import serializers
from . import repositories
from utils.paginations import CustomPagination
from .tasks import send_mail_to, expirate_task_to, notify_subscribers

VERSION = '1.7.2'


def hello(request):
    return HttpResponse(f'🌻 Hello Yuhu, by Osvaldo Hinojosa (v{VERSION}) 🚀')


def home(request):
    context = {
        'tasks': repositories.get_all_tasks(),
        'version': VERSION
    }
    return render(
        request,
        'task_home.html',
        context=context
    )


class Task(APIView):
    # permission_classes = [HasAPIKey]
    pagination_class = CustomPagination

    def get(self, request):
        paginator = self.pagination_class()

        # obtener las tareas que no han expirado
        tasks = repositories.get_all_tasks()
        result_page = paginator.paginate_queryset(tasks, request)
        serializer = serializers.Task(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    def post(self, request):
        serializer = serializers.Task(data=request.data)
        if serializer.is_valid():
            serializer.save()

            data = serializer.data
            date_expiration = data.get('date_expiration')
            id = data.get('id')
            email = data.get('email')
            title = data.get('title')
            is_public = data.get('is_public')

            # si se recibe un date_expiration usar celery para programar eliminar la tarea
            if date_expiration:
                _date_expiration = datetime.strptime(
                    date_expiration,
                    '%Y-%m-%dT%H:%M:%S%z'
                )
                expirate_task_to.apply_async(
                    args=[id],
                    eta=_date_expiration
                )

            if is_public:
                notify_subscribers.delay(id)

            send_mail_to.delay(
                email,
                'Nueva tarea agregada',
                f'Se ha creado con exito la tarea "{title}".'
            )
            return Response(data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            task = TaskModel.objects.get(pk=pk)
        except TaskModel.DoesNotExist:
            return Response('Register not found', status=status.HTTP_404_NOT_FOUND)

        task.delete()

        # regresar el pk del registro eliminado
        return Response({'pk': pk}, status=status.HTTP_204_NO_CONTENT)

    def patch(self, request, pk):
        try:
            task = TaskModel.objects.get(pk=pk)
        except TaskModel.DoesNotExist:
            return Response('Register not found', status=status.HTTP_404_NOT_FOUND)

        # que no se pueda modificar el email
        request.data.pop('email', None)
        request.data.pop('user', None)
        request.data.pop('date_expiration', None)

        serializer = serializers.Task(task, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()

            # emviar correo de notificacion
            send_mail_to.delay(
                task.email,
                'Tarea modificada',
                'La tarea "{}" ha sido modificada.'.format(task.title)
            )

            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MyTask(APIView):
    pagination_class = CustomPagination
    permission_classes = [IsAuthenticated]

    def get(self, request):
        pk = request.user.pk

        paginator = self.pagination_class()

        # obtener las tareas que no han expirado de un usuario
        tasks = TaskModel.objects.filter(
            is_expired=False, user_id=pk
        ).order_by('-pk')
        result_page = paginator.paginate_queryset(tasks, request)
        serializer = serializers.Task(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    def post(self, request):
        serializer = serializers.Task(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user, email=request.user.email)

            data = serializer.data

            # si se recibe un date_expiration usar celery para programar eliminar la tarea
            if data.get('date_expiration'):
                date_expiration = datetime.strptime(
                    data['date_expiration'],
                    '%Y-%m-%dT%H:%M:%S%z'
                )

                expirate_task_to.apply_async(
                    args=[data['id']],
                    eta=date_expiration
                )

            # enviar email usando celery
            send_mail_to.delay(
                data['email'],
                'Nueva tarea agregada',
                'Se ha creado con exito la tarea "{}".'.format(
                    data['title'])
            )

            return Response(data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
