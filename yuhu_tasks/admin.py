from django.contrib import admin

from .models import Task as TaskModel, Topic as TopicModel, Subscriber as SubscriberModel


class TaskAdmin(admin.ModelAdmin):
    list_display = [
        'pk',
        'title',
        'email',
        'description',
        'user',
        'date_expiration',
        'get_topics',
        'is_expired',
        'is_public',
    ]

    def get_topics(self, obj):
        return "\n".join([p.name for p in obj.topics.all()])


class TopicAdmin(admin.ModelAdmin):
    list_display = [
        'pk',
        'name',
    ]


class SubscriberAdmin(admin.ModelAdmin):
    list_display = [
        'pk',
        'user',
        'get_topics',
    ]

    def get_topics(self, obj):
        return "\n".join([p.name for p in obj.topics.all()])


admin.site.register(TaskModel, TaskAdmin)
admin.site.register(TopicModel, TopicAdmin)
admin.site.register(SubscriberModel, SubscriberAdmin)
